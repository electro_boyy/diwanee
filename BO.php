<html>
    <head>
        <meta charset="UTF-8">
        <title> Diwanee Test </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="back-end test">
        <meta name="author" content="Bojan_V">

        <link rel="stylesheet" type="text/css" href="Style/nav.css"/>
        <link rel="stylesheet" type="text/css" href="Style/main.css"/>
    </head>
    <?php
    require_once 'Database.php';
    session_start();

    $name1 = $_POST['name'];
    $password1 = sha1($_POST['password']);

    if ((!isset($name1)) || (!isset($password1))) {
        echo "Please enter your Name and Password";
    } else {
        $connection = new Database();                                // Create new connection with database

        $name = $connection->prevent_sql_injection($name1);          // Prevent for sql injection
        $password = $connection->prevent_sql_injection($password1);  // Prevent for sql injection

        $Check = "SELECT count(*) FROM users WHERE
       Name ='" . $name . "' and
       Password ='" . $password . "'";

        $resultCheck = $connection->query($Check);                  // Check name and password
        $resultCheck1 = $connection->fetchRow($resultCheck);
        $resultCheck2 = $resultCheck1[0];
        if ($resultCheck2 == 0) {
            header("Location: index.php");                           // That user not exist or wrong password
        } else {
            $_SESSION['regUser'] = $name;
            header("Location: Home.php");                            // User exist and go to Home page
        }
    }
    ?>

</html>



