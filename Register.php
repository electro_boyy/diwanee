<html>
    <head>
        <meta charset="UTF-8">
        <title> Diwanee Test </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="back-end test">
        <meta name="author" content="Bojan_V">

        <link rel="stylesheet" type="text/css" href="Style/nav.css"/>
        <link rel="stylesheet" type="text/css" href="Style/main.css"/>
    </head>
    <body>
        <div class="register">
            <?php
            require_once 'Database.php';
            session_start();

            $nameReg1 = $_POST['nameReg'];
            $passReg1 = $_POST['passReg'];
            $passReg11 = $_POST['passReg1'];

            if (!empty($nameReg1) && !empty($passReg1) && !empty($passReg11)) {

                $passReg1 = sha1($_POST['passReg']);
                $passReg11 = sha1($_POST['passReg1']);

                if ($passReg1 === $passReg11) {

                    $connection = new Database();                                  // Create new connection with database

                    $nameReg = $connection->prevent_sql_injection($nameReg1);      // Prevent for sql injection
                    $passReg = $connection->prevent_sql_injection($passReg1);      // Prevent for sql injection 
                    $passReg1 = $connection->prevent_sql_injection($passReg11);    // Prevent for sql injection 

                    $existingUsers = "select count(*) from users where
                   Name ='" . $nameReg . "'";

                    $checkExisting = $connection->query($existingUsers);           // Check existing users
                    $resultCheck = $connection->fetchRow($checkExisting);
                    $result = $resultCheck[0];
                    if ($result == 0) {                                            // User does not exist
                        $regUser = "INSERT INTO users (Name,Password,Reg_date) VALUES ('" . $nameReg . "' , '" . $passReg . "' , NOW() )";
                        $reg = $connection->query($regUser);
                        if (!$reg) {
                            echo " Error ! Try again";
                        } else {
                            ?>
                            <ul>
                                <li><a href="Home.php">Home</a></li>
                            </ul>
                            <br>
                            <?php
                            $_SESSION['regUser'] = $nameReg;
                            echo "Thanks for registration $nameReg";
                        }
                    } else {                                                      // User exist
                        ?>
                        <ul>
                            <li><a href="index.php">Log In</a></li>
                        </ul>
                        <br>
                        <?php
                        echo "You are already registered user";
                    }
                } else {
                    ?>
                    <ul>
                        <li><a href="index.php">Back</a></li>
                    </ul>
                    <br>
                    <?php
                    echo "Confirm password again";
                }
            } else {
                header("Location: index.php");
            }
            ?>
        </div>
    </body>
</html>
