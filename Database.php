<?php

class Database {

    private $connection;

    public function __construct() {
        $config = parse_ini_file("config/db.ini");                            // Configuration of Database

        $this->connection = mysqli_connect($config['server'], $config['user'], $config['password'], $config['database']);
        if ($this->connection === false) {
            throw new Exception('Failed to connect to database' . mysqli_error($this->connection), 1);
        }
    }

    public function prevent_sql_injection($str) {
        return mysqli_real_escape_string($this->connection, $str);
    }

    public function query($query) {
        return mysqli_query($this->connection, $query);
    }

    public function fetchRow($query) {
        return mysqli_fetch_row($query);
    }

    public function fetchRows($query) {
        $result = $this->query($query);
        $a = array();
        while (($row = mysqli_fetch_assoc($result)) !== null) {
            $a[] = $row;
        }
        return $a;
    }

}
