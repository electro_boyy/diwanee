<html>
    <head>
        <meta charset="UTF-8">
        <title> Diwanee Test </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="back-end test">
        <meta name="author" content="Bojan_V">

        <link rel="stylesheet" type="text/css" href="Style/main.css"/>
        <link rel="stylesheet" type="text/css" href="Style/nav.css"/>
    </head>
    <body>
        <?php
        require_once 'Database.php';
        session_start();
        if (!empty($_SESSION['regUser'])) {                             // User must be logged to see list
            ?>
            <ul>
                <li><a href="Home.php">Home</a></li>
                <li><a href="ListAllUsers.php">List All Users</a></li>
                <li><a href="LogOut.php">Log Out</a></li>
            </ul>
            <br>
            <?php
            $connection = new Database();

            $listQuery = 'SELECT Name,Reg_date FROM users ORDER BY Reg_date DESC';
            $listing = $connection->fetchRows($listQuery);
            $numRows = count($listing);

            if ($numRows !== 0) {
                ?>
                <div class='listAllusers'>
                    <table> 
                        <tr>
                            <td> No </td>
                            <td> Name </td>
                            <td> Date of registrated </td>
                        </tr>
                        <?php
                        $i = 0;
                        $n = 1;
                        while ($i < $numRows) {
                            echo "<tr>";
                            echo "<td>";
                            echo $n;          // Number of rows in list
                            echo "</td>";
                            echo "<td>";
                            echo $listing[$i]['Name'];
                            echo "</td>";
                            echo "<td>";
                            echo $listing[$i]['Reg_date'];
                            echo "</td>";
                            echo "</tr>";

                            $n++;
                            $i++;
                        }
                        ?> 
                    </table>
                    <?php
                } else {
                    echo "List is empty";
                }
            } else {
                ?>
            </div>
            <ul>
                <li> <a class="active" href="index.php"> Log In </a> </li>
            </ul>
            <br>
            <div class="listAllusers">
                <?php
                echo "You must be a user to see that list";
            }
            ?>
        </div>
    </body>
</html>

