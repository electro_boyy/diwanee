<html>
    <head>
        <meta charset="UTF-8">
        <title> Diwanee Test </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="back-end test">
        <meta name="author" content="Bojan_V">

        <link rel="stylesheet" type="text/css" href="Style/nav.css"/>
        <link rel="stylesheet" type="text/css" href="Style/main.css"/>
    </head>
    <body>
        <ul>
            <li><a href="index.php"> Log In </a></li>
        </ul>
        <br>
        <div class="logOut">
            <?php
            session_start();
            if (isset($_SESSION['regUser'])) {
                unset($_SESSION['regUser']);
                session_destroy();
                echo " You are Log Out, Thanks for come";
            } else {
                echo " You were logged out ";
            }
            ?>
        </div>
    </body>
</html>

