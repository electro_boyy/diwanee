<html>
    <head>
        <meta charset="UTF-8">
        <title> Diwanee Test </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="back-end test">
        <meta name="author" content="Bojan_V">

        <link rel="stylesheet" type="text/css" href="Style/nav.css"/>
        <link rel="stylesheet" type="text/css" href="Style/main.css"/>
    </head>
    <body>
        <?php
        session_start();
        if (!empty($_SESSION['regUser'])) {
            ?>
            <ul>
                <li><a href="Home.php">Home</a></li>
                <li><a href="ListAllUsers.php">List All Users</a></li>
                <li><a href="LogOut.php">Log Out</a></li>
            </ul>
            <br>
            <?php
            echo '<div class="hello"> Hello ' . $_SESSION['regUser'] . '</div>';
        } else {
            ?>
            <ul>
                <li><a class="active" href="index.php"> Log In </a></li>
            </ul>
            <br>
            <?php
            echo '<div class="hello"> You must be user to see Home page </div>';
        }
        ?>
    </body>
</html>
